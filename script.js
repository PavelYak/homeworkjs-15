$(document).ready(function () {
    $(".menu-bar").on("click", "a", function (event) {
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});

const button = $('.scroll-top');

$(window).scroll(() => {
    const windowHeight = $(window).innerHeight();
    const offsetTop = $(window).scrollTop();

    if (offsetTop > windowHeight) {
        button.fadeIn();
    } else {
        button.fadeOut();
    }
});

button.click(() => {
    $(document.body.parentElement).animate({scrollTop: 0}, 1000);
});

const showNewsBtn = $('.show-img-btn');
$(showNewsBtn).click(function () {
    $(".top-rated").slideToggle("slow");
});